package com.test.technology.entity;

import lombok.Data;

@Data
public class TestVO {
    private long idx;
    private String userName;
}
