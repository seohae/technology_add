package com.test.technology.controllers;

import com.test.technology.commons.Output;
import com.test.technology.entity.TestVO;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Api(tags = {"TestController"})
@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    private final Output output;

    public TestController(Output output) {
        this.output = output;
    }

    @GetMapping("")
    public ResponseEntity<?> test(HttpServletRequest request) {
        TestVO testVO = new TestVO();
        testVO.setIdx(1);
        testVO.setUserName("kimseohae");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(testVO);
    }

    @GetMapping(value = "/test")
    public ResponseEntity<?> responseEntityTest() {
        TestVO testVO = new TestVO();
        testVO.setIdx(1);
        testVO.setUserName("kimseohae");
        return output.send(testVO);
    }
}
